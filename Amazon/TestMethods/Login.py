from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import time
from Amazon.TestMethods.ByLocators import ByLocators
class Login():
    def __init__(self,driver):
        self.driver = driver

    def path(self,username,password):
        self.username = username
        self.password = password

    def Sign_in(self):
        Byloc: ByLocators = ByLocators(self)
        time.sleep(10)
        Mov_to_Signin = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(Byloc.Movetosignin))
        time.sleep(5)
        ActionChains(self.driver).move_to_element(Mov_to_Signin).click(Mov_to_Signin).perform()

    def Login_to_Amazon(self):

        #Finding the username text and passing it
        self.driver.find_element_by_xpath("//input[@id='ap_email']").send_keys(self.username)
        time.sleep(3)

        #Finding the contunie button and clicking on it
        move_to_contunie = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@id='continue']")))
        time.sleep(3)
        ActionChains(self.driver).move_to_element(move_to_contunie).click(move_to_contunie).perform()

        #Finding the password textbox and passing  the password in it
        self.driver.find_element_by_xpath("//input[@id='ap_password']").send_keys(self.password)

        #Finding_Loginbutton_and_clicking_onit
        Mov_to_Signinbutton = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.ID, "signInSubmit")))
        time.sleep(5)
        ActionChains(self.driver).move_to_element(Mov_to_Signinbutton).click(Mov_to_Signinbutton).perform()
        time.sleep(5)


    def search_items(self,searchtext):
        move_to_seaechbox =WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.XPATH, "//input[@id='twotabsearchtextbox']")))
        time.sleep(2)
        ActionChains(self.driver).move_to_element(move_to_seaechbox).click(move_to_seaechbox).send_keys(searchtext,Keys.ENTER).perform()
        time.sleep(5)

        Mov_to_Sort = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located((By.XPATH, "//select[@id='sort']")))
        time.sleep(2)
        select =Select(Mov_to_Sort)
        select.select_by_visible_text("Price: High to Low")
        time.sleep(10)